package com.ubs.opsit.interviews.infra;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class BerlinClockTest {

    private BerlinClock berlinClock;

    @Before
    public void setUp() throws Exception {
        this.berlinClock = new BerlinClock();
    }

    @Test
    public void testConvertTime() throws Exception {

        String theTime = "13:17:01";

        assertThat(berlinClock.convertTime(theTime)).isEqualTo("O" + System.lineSeparator() +
                "RROO" + System.lineSeparator() +
                "RRRO" + System.lineSeparator() +
                "YYROOOOOOOO" + System.lineSeparator() +
                "YYOO");
    }



    @Test(expected = TimeValidatorException.class)
    public void testInputValidation1() throws Exception {
        String theTime = "43:17:01";

        berlinClock.convertTime(theTime);
    }

    @Test(expected = TimeValidatorException.class)
    public void testInputValidation2() throws Exception {
        String theTime = "13-17:01";

        berlinClock.convertTime(theTime);
    }
}