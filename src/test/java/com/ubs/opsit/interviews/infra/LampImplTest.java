package com.ubs.opsit.interviews.infra;

import com.ubs.opsit.interviews.infra.LampImpl;
import com.ubs.opsit.interviews.model.Color;
import com.ubs.opsit.interviews.model.TimeUnitConfigurations;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;

public class LampImplTest {

    private  List<LampImpl> lamps;

    @Before
    public void setUp() throws Exception {
        this.lamps = Arrays.asList(new LampImpl(Color.RED, TimeUnitConfigurations.fiveHourValue),
                new LampImpl(Color.RED, TimeUnitConfigurations.fiveMinuteValue),
                new LampImpl(Color.RED, TimeUnitConfigurations.oneHourValue),
                new LampImpl(Color.RED, TimeUnitConfigurations.oneMinuteValue),
                new LampImpl(Color.YELLOW, TimeUnitConfigurations.fiveHourValue),
                new LampImpl(Color.YELLOW, TimeUnitConfigurations.fiveMinuteValue),
                new LampImpl(Color.YELLOW, TimeUnitConfigurations.oneHourValue),
                new LampImpl(Color.YELLOW, TimeUnitConfigurations.oneMinuteValue));
    }

    @Test
    public void testLampsOff() throws Exception {
        Assert.assertThat(printLampArray(),is("OOOOOOOO"));
    }

    @Test
    public void testLampsOn() throws Exception {
        for (LampImpl lamp : lamps) {
           lamp.setLightOn();
        }
        Assert.assertThat(printLampArray(),is("RRRRYYYY"));
    }

    private String printLampArray() {
        StringBuilder sb = new StringBuilder();

        for (LampImpl lamp : lamps) {
            sb.append(String.valueOf(lamp));
        }
        return sb.toString();
    }
}