package com.ubs.opsit.interviews.infra;

import com.ubs.opsit.interviews.model.LampModule;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;

public class TimeInterpreterTest {

    private LampModule hourLampModule;
    private LampModule minuteLampModule;
    private LampModule secondLampModule;

    @Before
    public void setUp() throws Exception {
        this.hourLampModule = new HourLampModule();
        this.minuteLampModule = new MinuteLampModule();
        this.secondLampModule = new SecondLampModule();
    }

    @Test
    public void testTimeInterpreter() throws Exception {
        TimeInterpreter timeInterpreter = new TimeInterpreter();
        timeInterpreter.interpretTime(hourLampModule,13);
        timeInterpreter.interpretTime(minuteLampModule,17);
        timeInterpreter.interpretTime(secondLampModule,1);

        StringBuilder sb = new StringBuilder();
        sb.append(secondLampModule.print());
        sb.append(System.lineSeparator());
        sb.append(hourLampModule.print());
        sb.append(System.lineSeparator());
        sb.append(minuteLampModule.print());

        Assert.assertThat(sb.toString(),is("O" + System.lineSeparator() +
                "RROO" + System.lineSeparator() +
                "RRRO" + System.lineSeparator() +
                "YYROOOOOOOO" + System.lineSeparator() +
                "YYOO"));
    }
}