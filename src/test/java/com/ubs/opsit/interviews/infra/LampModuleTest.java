package com.ubs.opsit.interviews.infra;

import com.ubs.opsit.interviews.model.LampModule;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;

public class LampModuleTest {

    private LampModule hourLampModule;
    private LampModule minuteLampModule;
    private LampModule secondLampModule;

    @Before
    public void setUp() throws Exception {
        this.hourLampModule = new HourLampModule();
        this.minuteLampModule = new MinuteLampModule();
        this.secondLampModule = new SecondLampModule();
    }

    @Test
    public void testLampModules() throws Exception {

        StringBuilder sb = new StringBuilder();

        sb.append(secondLampModule.print());
        sb.append(System.lineSeparator());
        sb.append(hourLampModule.print());
        sb.append(System.lineSeparator());
        sb.append(minuteLampModule.print());

        Assert.assertThat(sb.toString(),is("O" + System.lineSeparator() +
                "OOOO" + System.lineSeparator() +
                "OOOO" + System.lineSeparator() +
                "OOOOOOOOOOO" + System.lineSeparator() +
                "OOOO"));
    }
}