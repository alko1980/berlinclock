package com.ubs.opsit.interviews.infra;

import com.ubs.opsit.interviews.model.*;

import java.util.Arrays;
import java.util.List;

public class HourLampModule implements LampModule {

    private final List<Lamp> lamps;

    public HourLampModule() {
        this.lamps = Arrays.asList(new LampImpl(Color.RED, TimeUnitConfigurations.fiveHourValue),
                new LampImpl(Color.RED, TimeUnitConfigurations.fiveHourValue),
                new LampImpl(Color.RED, TimeUnitConfigurations.fiveHourValue),
                new LampImpl(Color.RED, TimeUnitConfigurations.fiveHourValue),

                new LampImpl(Color.RED, TimeUnitConfigurations.oneHourValue),
                new LampImpl(Color.RED, TimeUnitConfigurations.oneHourValue),
                new LampImpl(Color.RED, TimeUnitConfigurations.oneHourValue),
                new LampImpl(Color.RED, TimeUnitConfigurations.oneHourValue));
    }

    @Override
    public List<Lamp> getLamps() {
        return lamps;
    }

    @Override
    public String print() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 4; i++) {
            sb.append(lamps.get(i).toString());
        }
        sb.append(System.lineSeparator());
        for (int i = 4; i < 8; i++) {
            sb.append(lamps.get(i).toString());
        }
        return sb.toString();
    }
}
