package com.ubs.opsit.interviews.infra;

import com.ubs.opsit.interviews.model.Color;
import com.ubs.opsit.interviews.model.Lamp;
import com.ubs.opsit.interviews.model.LampModule;
import com.ubs.opsit.interviews.model.TimeUnitConfigurations;

import java.util.Arrays;
import java.util.List;

public class SecondLampModule implements LampModule {

    private final List<Lamp> lamps;

    public SecondLampModule() {
        this.lamps = Arrays.asList(
                new LampImpl(Color.YELLOW, TimeUnitConfigurations.oneSecondValue));
    }
    public List<Lamp> getLamps() {
        return lamps;
    }

    @Override
    public String print() {
        return lamps.get(0).toString();
    }
}
