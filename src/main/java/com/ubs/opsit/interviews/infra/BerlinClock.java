package com.ubs.opsit.interviews.infra;

import com.ubs.opsit.interviews.model.*;

public class BerlinClock implements TimeConverter {

    private LampModule hourLampModule;
    private LampModule minuteLampModule;
    private LampModule secondLampModule;

    private TimeValidator timeValidator;

    public BerlinClock() {
        this.hourLampModule = new HourLampModule();
        this.minuteLampModule = new MinuteLampModule();
        this.secondLampModule = new SecondLampModule();
        this.timeValidator = new TimeValidatorImpl();
    }

    @Override
    public String convertTime(String aTime) {

        timeValidator.validate(aTime);

        String[] split = aTime.split(":");

        TimeInterpreter timeInterpreter = new TimeInterpreter();
        timeInterpreter.interpretTime(hourLampModule, Integer.valueOf(split[0]));
        timeInterpreter.interpretTime(minuteLampModule, Integer.valueOf(split[1]));
        timeInterpreter.interpretTime(secondLampModule, Integer.valueOf(split[2]));
        StringBuilder sb = new StringBuilder();

        sb.append(secondLampModule.print());
        sb.append(System.lineSeparator());
        sb.append(hourLampModule.print());
        sb.append(System.lineSeparator());
        sb.append(minuteLampModule.print());

        return sb.toString();
    }
}
