package com.ubs.opsit.interviews.infra;

import com.ubs.opsit.interviews.model.Color;
import com.ubs.opsit.interviews.model.Lamp;

public class LampImpl implements Lamp {

    private boolean isLightOn;
    private final Color color;
    private final LampValue lampValue;

    public LampImpl(Color color, LampValue lampValue) {
        this.color = color;
        this.lampValue = lampValue;
    }

    @Override
    public void setLightOn() {
        isLightOn = true;
    }

    @Override
    public void setLightOff() {
        isLightOn = false;
    }

    private String getColor(){
        if(isLightOn){
            return color.toString();
        }
        return "O";
    }

    @Override
    public String toString() {
        return getColor();
    }

    @Override
    public int getAmount() {
        return lampValue.getAmount();
    }
}
