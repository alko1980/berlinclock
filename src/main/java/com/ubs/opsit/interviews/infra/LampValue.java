package com.ubs.opsit.interviews.infra;


import com.ubs.opsit.interviews.model.TimeUnit;
import com.ubs.opsit.interviews.model.TimeValue;

public class LampValue  implements TimeValue {

   private final TimeUnit timeUnit;
   private final int amount;

    public LampValue(TimeUnit timeUnit, int amount) {
        this.timeUnit = timeUnit;
        this.amount = amount;
    }

    @Override
    public int getAmount() {
        return amount;
    }
}
