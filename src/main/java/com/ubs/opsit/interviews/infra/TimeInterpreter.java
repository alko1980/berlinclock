package com.ubs.opsit.interviews.infra;

import com.ubs.opsit.interviews.model.Lamp;
import com.ubs.opsit.interviews.model.LampModule;

import java.util.List;

public class TimeInterpreter {

    public void interpretTime(LampModule module, int time) {
        if (module instanceof SecondLampModule) {
            interpretSeconds((SecondLampModule) module, time);
            return;
        }
        interpret(module, time);
    }

    private void interpretSeconds(SecondLampModule module, int time) {
        List<Lamp> lamps = module.getLamps();

        if (time % 2 == 0) {
            lamps.get(0).setLightOn();
            return;
        }
        lamps.get(0).setLightOff();
    }

    private void interpret(LampModule module, int time) {
        calculateLampValues(module, time);
    }

    private void calculateLampValues(LampModule module, int time) {
        List<Lamp> lamps = module.getLamps();

        clean(lamps);

        if (time < 5) {
            oneTimeUnitCalculator(time, lamps, 1);
        }

        int qoutient = time / 5;
        int reminder = time % 5;

        oneTimeUnitCalculator(qoutient, lamps, 5);

        oneTimeUnitCalculator(reminder, lamps, 1);
    }

    private void oneTimeUnitCalculator(int time, List<Lamp> lamps, int amount) {
        int counter = 0;
        for (Lamp lamp : lamps) {
            if (counter < time && lamp.getAmount() == amount) {
                lamp.setLightOn();
                counter++;
            }
        }
    }

    private void clean(List<Lamp> lamps) {
        lamps.forEach(Lamp::setLightOff);
    }
}
