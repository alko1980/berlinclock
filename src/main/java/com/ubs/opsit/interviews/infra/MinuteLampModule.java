package com.ubs.opsit.interviews.infra;

import com.ubs.opsit.interviews.model.Color;
import com.ubs.opsit.interviews.model.Lamp;
import com.ubs.opsit.interviews.model.LampModule;
import com.ubs.opsit.interviews.model.TimeUnitConfigurations;

import java.util.Arrays;
import java.util.List;

public class MinuteLampModule implements LampModule {

    private final List<Lamp> lamps;

    public MinuteLampModule() {
        this.lamps = Arrays.asList(
                new LampImpl(Color.YELLOW, TimeUnitConfigurations.fiveMinuteValue),
                new LampImpl(Color.YELLOW, TimeUnitConfigurations.fiveMinuteValue),
                new LampImpl(Color.RED, TimeUnitConfigurations.fiveMinuteValue),
                new LampImpl(Color.YELLOW, TimeUnitConfigurations.fiveMinuteValue),
                new LampImpl(Color.YELLOW, TimeUnitConfigurations.fiveMinuteValue),
                new LampImpl(Color.RED, TimeUnitConfigurations.fiveMinuteValue),
                new LampImpl(Color.YELLOW, TimeUnitConfigurations.fiveMinuteValue),
                new LampImpl(Color.YELLOW, TimeUnitConfigurations.fiveMinuteValue),
                new LampImpl(Color.RED, TimeUnitConfigurations.fiveMinuteValue),
                new LampImpl(Color.YELLOW, TimeUnitConfigurations.fiveMinuteValue),
                new LampImpl(Color.YELLOW, TimeUnitConfigurations.fiveMinuteValue),

                new LampImpl(Color.YELLOW,  TimeUnitConfigurations.oneMinuteValue),
                new LampImpl(Color.YELLOW, TimeUnitConfigurations.oneMinuteValue),
                new LampImpl(Color.YELLOW, TimeUnitConfigurations.oneMinuteValue),
                new LampImpl(Color.YELLOW, TimeUnitConfigurations.oneMinuteValue));
    }

    public List<Lamp> getLamps() {
        return lamps;
    }

    @Override
    public String print() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 11; i++) {
            sb.append(lamps.get(i).toString());
        }
        sb.append(System.lineSeparator());
        for (int i = 11; i < 15; i++) {
            sb.append(lamps.get(i).toString());
        }
        return sb.toString();
    }
}
