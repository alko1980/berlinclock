package com.ubs.opsit.interviews.infra;

import com.ubs.opsit.interviews.model.TimeValidator;

public class TimeValidatorImpl implements TimeValidator {

    @Override
    public void validate(String time) {
        try {
            if (time == null) {
                throw new TimeValidatorException("Time string is null");
            }
            String[] split = time.split(":");

            int hours = Integer.valueOf(split[0]);
            int minutes = Integer.valueOf(split[1]);
            int seconds = Integer.valueOf(split[2]);

            if (hours < 0 || hours > 24) {
                throw new TimeValidatorException("Time hours value should be in range 0-24");
            }
            if (minutes < 0 || minutes > 59) {
                throw new TimeValidatorException("Time minutes value should be in range 0-59");
            }
            if (seconds < 0 || seconds > 59) {
                throw new TimeValidatorException("Time seconds value should be in range 0-59");
            }
        } catch (Exception ex) {
            throw new TimeValidatorException(String.format("Time $s is Not valid", time));
        }
    }
}
