package com.ubs.opsit.interviews.infra;

public class TimeValidatorException extends RuntimeException {

    public TimeValidatorException(String message) {
        super(message);
    }
}
