package com.ubs.opsit.interviews.model;

import java.util.List;

public interface LampModule {
   List<Lamp> getLamps();

   String print();
}
