package com.ubs.opsit.interviews.model;

import com.ubs.opsit.interviews.infra.LampValue;

public interface TimeUnitConfigurations {

     LampValue fiveHourValue = new LampValue(TimeUnit.HOUR, 5);
     LampValue fiveMinuteValue = new LampValue(TimeUnit.MINUTE, 5);
     LampValue oneHourValue = new LampValue(TimeUnit.HOUR, 1);
     LampValue oneMinuteValue = new LampValue(TimeUnit.MINUTE, 1);
     LampValue oneSecondValue = new LampValue(TimeUnit.SECOND, 1);
}
