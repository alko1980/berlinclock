package com.ubs.opsit.interviews.model;

public interface TimeValue {
    int getAmount();
}
