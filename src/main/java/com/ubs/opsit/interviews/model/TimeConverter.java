package com.ubs.opsit.interviews.model;

public interface TimeConverter {
    String convertTime(String aTime);
}
