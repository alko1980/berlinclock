package com.ubs.opsit.interviews.model;

public interface TimeValidator {
    void validate(String time);
}
