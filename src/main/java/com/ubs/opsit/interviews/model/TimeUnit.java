package com.ubs.opsit.interviews.model;

public enum TimeUnit {
    HOUR,
    MINUTE,
    SECOND
}
