package com.ubs.opsit.interviews.model;

public enum Color {

    YELLOW ("Y"),
    RED ("R");

    private String code;

    Color(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return code;
    }
}
