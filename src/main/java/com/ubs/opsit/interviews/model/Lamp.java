package com.ubs.opsit.interviews.model;

public interface Lamp extends TimeValue {

    void setLightOn();

    void setLightOff();
}
